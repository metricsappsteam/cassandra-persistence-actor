package es.kibu.geoapis.backend.actors.persistence;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.JavaTestKit;
import es.kibu.geoapis.backend.actors.messages.Result;
import junit.framework.TestCase;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by lrodriguez2002cu on 25/04/2017.
 */
public class CassandraPersistenceActorTest {


    private final String targetCollection = "\"metrics\".\"var_movementspeedMetricsNacho\"";
    Logger logger = LoggerFactory.getLogger(CassandraPersistenceActorTest.class);

    static ActorSystem system;
    static CassandraPersistenceActor.CassandraSettings settings;

    @BeforeClass
    public static void setup() {
        system = ActorSystem.apply("TestActorSystem");
        settings = new CassandraPersistenceActor.CassandraSettings(){
            @Override
            public String getCassandraDBUser() {
                return null;
            }

            @Override
            public String getCassandraDBPass() {
                return null;
            }

            @Override
            public String getCassandraDBName() {
                return "metrics";
            }

            @Override
            public String getCassandraDBUri() {
                return "geobd03.init.uji.es";
            }

            @Override
            public void setCassandraDBName(String cassandraDBName) {

            }

            @Override
            public void setCassandraDBUri(String cassandraDBUri) {

            }
        };

    }


    @Test
    public void testCassandraActor(){

        AbstractQueries.Query  q = new AbstractQueries.SelectQuery(String.class, targetCollection);

        testQuery(q, new QueryResultValidator() {
            @Override
            public void validateResult(Result resultMessage) {
                Assert.assertTrue(resultMessage.isSuccess());
                logger.debug(resultMessage.getResultValue().toString());
            }
        });
    }


    @Test
    public void testCassandraActorFilters(){

        AbstractQueries.Query  q = new AbstractQueries.SelectQuery(String.class, targetCollection);
        q.withFilter("user", AbstractQueries.ComparisonOperators.EQUAL, "user1");
        q.withFilter("application", AbstractQueries.ComparisonOperators.EQUAL, "app-47f2e1b5a3c44404");
        q.withFilter("session", AbstractQueries.ComparisonOperators.EQUAL, "session1");

        testQuery(q, new QueryResultValidator() {
            @Override
            public void validateResult(Result resultMessage) {
                Assert.assertTrue(resultMessage.isSuccess());
                logger.debug(resultMessage.getResultValue().toString());
            }
        });
    }


    @Test
    public void testCassandraActorFiltersTime(){

        AbstractQueries.Query  q = new AbstractQueries.SelectQuery(String.class, targetCollection);
        q.withFilter("user", AbstractQueries.ComparisonOperators.EQUAL, "user1");
        q.withFilter("application", AbstractQueries.ComparisonOperators.EQUAL, "app-47f2e1b5a3c44404");
        q.withFilter("session", AbstractQueries.ComparisonOperators.EQUAL, "session1");
        q.withFilter("time", AbstractQueries.ComparisonOperators.LT, new AbstractQueries.QueryValue<DateTime>(DateTime.parse("2017-04-11T09:52:39.773Z")));

        testQuery(q, new QueryResultValidator() {
            @Override
            public void validateResult(Result resultMessage) {
                logger.debug("Result: {} output: {}" , resultMessage.isSuccess()? "success": "failure", resultMessage.isSuccess()? resultMessage.getResultValue().toString(): resultMessage.getMessage());
                Assert.assertTrue(resultMessage.isSuccess());
            }
        });
    }

    @Test
    public void testCassandraActorFiltersTimeSortByTime(){

        AbstractQueries.Query  q = new AbstractQueries.SelectQuery(String.class, targetCollection);
        q.withFilter("user", AbstractQueries.ComparisonOperators.EQUAL, "user1");
        q.withFilter("application", AbstractQueries.ComparisonOperators.EQUAL, "app-47f2e1b5a3c44404");
        q.withFilter("session", AbstractQueries.ComparisonOperators.EQUAL, "session1");
        q.withFilter("time", AbstractQueries.ComparisonOperators.LT, new AbstractQueries.QueryValue<DateTime>(DateTime.parse("2017-04-11T09:52:39.773Z")));
        q.sortBy("time", AbstractQueries.SortDirection.DESC);

        testQuery(q, new QueryResultValidator() {
            @Override
            public void validateResult(Result resultMessage) {
                logger.debug("Result: {} output: {}" , resultMessage.isSuccess()? "success": "failure", resultMessage.isSuccess()? resultMessage.getResultValue().toString(): resultMessage.getMessage());
                Assert.assertTrue(resultMessage.isSuccess());
            }
        });
    }




    @Test
    public void testCassandraActorSelectFail(){

        AbstractQueries.Query  q = new AbstractQueries.SelectQuery(String.class, "\"metrics\".\"kkk\"");

        testQuery(q, new QueryResultValidator() {
            @Override
            public void validateResult(Result resultMessage) {
                logger.debug(resultMessage.getResultValue().toString());
                Assert.assertFalse(resultMessage.isSuccess());
            }
        });
    }



    interface QueryResultValidator{
        void validateResult(Result resultMessage);
    }

    public void testQuery(AbstractQueries.QueryStatement q, QueryResultValidator validator) {
        new JavaTestKit(system) {{

            final ActorRef subject = system.actorOf(CassandraPersistenceActor.props(settings));
            // can also use JavaTestKit “from the outside”
            final JavaTestKit probe = new JavaTestKit(system);

            new Within(duration("10000 seconds")) {

                protected void run() {

                    subject.tell(q, getRef());

                    Result result = expectMsgClass(Result.class);

                    validator.validateResult(result);
                    return;
                    //expectNoMsg();
                }
            };
        }};
    }
}