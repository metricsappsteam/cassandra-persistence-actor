package es.kibu.geoapis.backend.actors.persistence;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by lrodriguez2002cu on 25/04/2017.
 */
public class CassandraQueryTranslatorTest {

    class TriggerCreateParams {

    }

    class LocationCreateParams {

    }

    @Test
    public void translateQuery() throws Exception {
        AbstractQueries.Query q = (AbstractQueries.Query) AbstractQueriesTest.getQueryStatement();
        translateAndCheck(q);
    }

    @Test
    public void translateSelectQuery() throws Exception {
        AbstractQueries.Query q = (AbstractQueries.Query) AbstractQueriesTest.getQuerySelectStatement2Filters();
        translateAndCheck(q);
    }

    @Test
    public void translateSelectQueryLocation() throws Exception {
        AbstractQueries.Query q = AbstractQueriesTest.getQuerySelectStatementForAppIdOnLocations("test_application", TriggerCreateParams.class);
        translateAndCheck(q);
    }

    @Test
    public void translateSelectQueryLocationWithElements() throws Exception {

        try {
            AbstractQueries.Query q = AbstractQueriesTest.getQuerySelectStatementForAppIdOnLocationsOnElements("test_application", LocationCreateParams.class);
            translateAndCheck(q);
            fail();
        } catch (Exception e) {
           //NOTE: this is the intended behavior as cassandra doent have the element match
        }
    }


    @Test
    public void translateSelectQueryById() throws Exception {
        AbstractQueries.Query q = (AbstractQueries.Query) AbstractQueriesTest
                .getQuerySelectStatementRangeFilterAndIdFilter("test_application", "location", TriggerCreateParams.class);
        translateAndCheck(q);
    }

    @Test
    public void translateSelectQueryByIdLimit50() throws Exception {
        AbstractQueries.Query q = (AbstractQueries.Query) AbstractQueriesTest
                .getQuerySelectStatementRangeFilterAndIdFilter("test_application", "location", TriggerCreateParams.class);
        q.limit(50);
        translateAndCheck(q);
    }

    private void checkHoldersAndParams(QueryStatementTranslator.QueryDef query) {
        int matches = StringUtils.countMatches(query.getFullQuery(), CassandraQueryTranslator.DefaultRenderer.PARAMETER_PLACEHOLDER);
        assertEquals(matches, query.getQueryParams().size());
    }


    @Test
    public void translateRangeQuery() throws Exception {
        AbstractQueries.Query q = (AbstractQueries.Query) AbstractQueriesTest.getQuerySelectStatementRangeFilterAndEqual(TriggerCreateParams.class);
        translateAndCheck(q);
    }

    public void translateAndCheck(AbstractQueries.Query q) {

        CassandraQueryTranslator translator = new CassandraQueryTranslator();

        /*translator.addPropertyRenderInterceptor(new QueryStatementTranslator.PropertyTemplateRenderInterceptor() {
            @Override
            public String renderPropertyTemplate(QueryStatementTranslator translator, String propertyName) {
                if (propertyName.equalsIgnoreCase(PersistenceMessageUtils.APPLICATION_ID) || propertyName.equalsIgnoreCase("_id")) {
                    //render it simple .. dont include a root property whatsoever
                    return "%s";
                }
                return null;
            }
        });*/

      /*  translator.addPropertyRenderInterceptor(new QueryStatementTranslator.PropertyValueRenderInterceptor() {
            @Override
            public String renderPropertyValue(QueryStatementTranslator translator, String propertyName, Object propertyValue) {
                if (propertyName.equalsIgnoreCase("_id")){
                    //render it simple .. dont include a root property whatsoever
                    return "{$oid:\"" + propertyValue + "\"}";
                }
                return null;
            }

        });
*/
        QueryStatementTranslator.QueryTranslatorResult queryTranslatorResult = translator.translateQuery(q);
        QueryStatementTranslator.QueryDef query = queryTranslatorResult.getQuery();
        assertTrue(!query.getFullQuery().isEmpty());
        checkHoldersAndParams(query);

        System.out.println(String.format("Query: %s", query.getFullQuery()));
    }


}