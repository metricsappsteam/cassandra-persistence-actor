/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright [ 2016] [lrodriguez2002cu]
 *
 */

package es.kibu.geoapis.backend.actors.persistence;

import akka.actor.Props;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import es.kibu.geoapis.backend.actors.FeatureNotSupportedException;
import es.kibu.geoapis.backend.actors.messages.CommandMessage;
import es.kibu.geoapis.backend.actors.messages.Result;
import es.kibu.geoapis.backend.actors.persistence.AbstractQueries.InsertQuery;
import es.kibu.geoapis.backend.actors.persistence.AbstractQueries.Query;
import es.kibu.geoapis.backend.actors.persistence.AbstractQueries.QueryType;
import es.kibu.geoapis.backend.actors.persistence.AbstractQueries.UpdateQuery;
import es.kibu.geoapis.backend.actors.serialization.SerializationUtils;
import org.jongo.Mapper;
import org.jongo.marshall.jackson.JacksonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.collection.Seq;
import scala.collection.mutable.ArraySeq;
import scala.reflect.ClassTag;

import java.util.ArrayList;
import java.util.List;

import static es.kibu.geoapis.backend.actors.persistence.Targets.AUTHORIZATION;

@SuppressWarnings("Duplicates")
public class CassandraPersistenceActor extends PersistenceActor {

    Logger logger = LoggerFactory.getLogger(CassandraPersistenceActor.class);

    public static final String SERIALIZATION_ACTOR = "serializationMongoActor";
    private static final String ENTITY = "entity";
    private static final String ID_FIELD = "_id";
    private static final String ENTITY_ID = ID_FIELD;

    //Mongo client
    private Session session;

    String keyspace;

    CassandraSettings settings;

    public interface CassandraSettings {
        String getCassandraDBUser();

        String getCassandraDBPass();

        String getCassandraDBName();

        String getCassandraDBUri();

        void setCassandraDBName(String cassandraDBName);

        void setCassandraDBUri(String cassandraDBUri);
    }

    public CassandraPersistenceActor(CassandraSettings settings) {
        this.settings = settings;
    }

    public CassandraPersistenceActor() {
    }

    public void init() {
        //Query
        keyspace = settings.getCassandraDBName();/*CASSANDRA_DB_NAME*/

        String query = String.format("CREATE KEYSPACE IF NOT EXISTS %s WITH REPLICATION ", keyspace)
                + "= {'class':'SimpleStrategy', 'replication_factor':1};";

        logger.debug("create query: {}", query);

        //creating Cluster object
      /*  String[] split = settings.CASSANDRA_DB_URI.split(":");
        String address = split[0];
        String port = split[1];
        Cluster cluster = Cluster.builder().addContactPointsWithPorts(new InetSocketAddress(address, Integer.parseInt(port))).build();
      */
        String cassandraHost = settings.getCassandraDBUri()/*CASSANDRA_DB_URI*/;
        Cluster cluster = Cluster.builder().addContactPoint(cassandraHost).build();
        //Creating Session object


        session = cluster.connect();

        //Executing the query
        session.execute(query);

        //using the KeySpace
        session.execute(String.format("USE %s", keyspace));
        logger.debug("Keyspace created: {}", keyspace);


        VisibilityChecker visibilityChecker = new SerializationUtils.GeofencesCustomVisibilityChecker(JsonAutoDetect.Visibility.PUBLIC_ONLY);
                   /*new VisibilityChecker.Std(JsonAutoDetect.Visibility.PUBLIC_ONLY)
                    .withVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.PUBLIC_ONLY)
                    .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                    .withIsGetterVisibility(JsonAutoDetect.Visibility.NONE);*/

        /*mapper = new JacksonMapper.Builder()
                .setVisibilityChecker(visibilityChecker)
                //.registerModule(new JodaModule())
                .registerModule(new SerializationUtils.GeofencesSerializationModule())
                .build();*/
        mapper = new ObjectMapper().registerModule(new SerializationUtils.GeofencesSerializationModule());
    }

    ObjectMapper mapper;

    public static Props props() {
        ClassTag<CassandraPersistenceActor> tag = scala.reflect.ClassTag$.MODULE$.apply(CassandraPersistenceActor.class);
        return Props.apply(tag);
    }

    public static Props props(CassandraSettings settings) {
        Class clazz = CassandraPersistenceActor.class;
        Props apply = Props.create(clazz,  settings);
        return apply;
    }

    @Override
    public void preStart() throws Exception {
        super.preStart();
        init();
    }

    public void onReceive(Object message) throws Exception {

        if (message instanceof CommandMessage) {
             //this is a deprecated way
            String receivedMessage = String.format("%s ( %s )", ((CommandMessage) message).getCommand().verb(), ((CommandMessage) message).getData().toString());
            log.info("In CassandraPersistenceActor - received message: {}", receivedMessage);

            //handle the message
            QueryResult result = handleMessage((CommandMessage) message);

            assert result != null;

            //get the result type
            Result.ResultType status = result.getResultType();

            //reply to the sender
            getSender().tell(createResultMsg(result), getSelf());

        } else {

            // if the received message is a Query
            if (message instanceof Query) {

                Query query = (Query) message;

                log.info("In PersistenceMongoActor - received message: {}", query);

                //handle the message
                QueryResult result = handleQuery(query);

                assert result != null;

                //get the result type
                Result.ResultType status = result.getResultType();

                //reply to the sender
                getSender().tell(createResultMsg(result), getSelf());

            } else {
                unhandled(message);
            }
        }
    }


    protected String getEntityId(Query query) {
        return getFilterProperty(query, ENTITY_ID);
    }

    public static final String APPLICATION = "application";

    protected String getApplicationId(Query query) {
        return getFilterProperty(query, APPLICATION);
    }

    protected String getUserId(Query query) {
        return getFilterProperty(query, USER_ID);
    }

    @Override
    protected QueryResult doDelete(Query query) throws Exception {

        //some assertions
        assert query.getQueryType() == QueryType.DELETE;

        String id = getFilterProperty(query, ENTITY_ID);
        assert id != null;

        String applicationId = getApplicationId(query);
        assert applicationId != null;

        String targetCollection = query.getTarget().getTargetName();
        assert targetCollection != null;

        QueryStatementTranslator.QueryDef queryDef = translateQuery(query);
        ResultSet result = session.execute(queryDef.getFullQuery(), queryDef.getQueryParams().toArray());

        //TODO: check if this is correct.
        return new DBDeleteResult(id, result.wasApplied());
        //throw new FeatureNotSupportedException("doDelete(Query query) is still not supported");
    }

    @Override
    protected QueryResult doUpdate(Query query) throws Exception {
        //some assertions
        assert query.getQueryType() == QueryType.UPDATE;

        String id = getEntityId(query);
        assert id != null;

        String applicationId = getApplicationId(query);
        assert applicationId != null;

        String targetCollection = query.getTarget().getTargetName();
        assert targetCollection != null;

        //remove the object from the collection
        Object entity = ((UpdateQuery) query).getEntity();
        QueryStatementTranslator.QueryDef queryDef = translateQuery(query);
        ResultSet result = session.execute(queryDef.getFullQuery(), queryDef.getQueryParams().toArray());

        String jsonInString = mapper.writeValueAsString(entity);

        //TODO: check if this is correct.
        //return new DBDeleteResult(id, result.wasApplied());
        return new DBUpdateResult(id, result.wasApplied(), -1);
        //throw new FeatureNotSupportedException("doUpdate(Query query) is still not suported");

    }

    /***
     * This is where the queries to the system occur.
     * @param query the query
     * @return a query result
     * @throws Exception
     */
    @Override
    protected QueryResult doSelection(Query query) throws Exception {

        //some assertions
        assert query.getQueryType() == QueryType.SELECT;

        /*String applicationId = getApplicationId(query);
        assert applicationId != null;*/

        String targetCollection = query.getTarget().getTargetName();
        assert targetCollection != null;

        QueryStatementTranslator.QueryDef queryDef = translateQuery(query);

        String fullQuery = queryDef.getFullQuery();

        log.debug("Query: {}", fullQuery);
        Object[] parameters = queryDef.getQueryParams().toArray(/*new Object[0]*/);

        List<Object> list = new ArrayList<>();

        try {
            ResultSet resultSet = session.execute(fullQuery, parameters);
            List<Row> all = resultSet.all();
            log.info("retrieving elements ({}) from database", all.size());
            for (Row row : all) {
                list.add(row.getString("[json]"));
            }

        } catch (Exception e) {
            //close the cursor.. always
            log.error(e, "Error while retrieving data from the cursor");
            return new DBExceptionResult(e);
        }

        return new DBReadResult(list);
    }

    private QueryStatementTranslator.QueryDef translateQuery(Query q) {

        QueryStatementTranslator translator = new CassandraQueryTranslator();
        QueryStatementTranslator.QueryTranslatorResult queryTranslatorResult = translator.translateQuery(q);
        QueryStatementTranslator.QueryDef queryDef = queryTranslatorResult.getQuery();
        return queryDef;

    }

    /*private String withApplicationId() {
        return String.format("{%s : #}", PersistenceActor.APPLICATION_ID);
    }*/

    /**
     * Insertions happen here..
     *
     * @param query the query containing the entity to insert
     * @return the result.
     * @throws Exception
     */
    @Override
    protected QueryResult doInsertion(Query query) throws Exception {
        //few assertions
        assert query.getQueryType() == QueryType.INSERT;

        //String applicationId = getApplicationId(query);
        //assert applicationId != null;

        String targetCollection = query.getTarget().getTargetName();
        assert targetCollection != null && !targetCollection.isEmpty();

        Object entity = ((InsertQuery) query).getEntity();
        assert entity != null;

        //if an insertion comes with an id.. it would be at the least suspicious
        //assert getEntityId(query) == null;

        QueryStatementTranslator.QueryDef queryDef = translateQuery(query);

        String jsonInString = mapper.writeValueAsString(entity);
        ResultSet result = session.execute(queryDef.getFullQuery(), jsonInString);

        return new DBWriteResult(result.one().toString());
        //throw new FeatureNotSupportedException("doInsert(Query query) is still not supported");

    }
}